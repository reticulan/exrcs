def recursive_append(dct: dict, search_key, new_item) -> None:
    for key, value in dct.items():
        if search_key == key:
            dct[key][new_item] = {}
            break
        else:
            recursive_append(dct[key], search_key, new_item)


def list_to_tree(source: list[tuple]) -> dict:
    new_dict = {}
    for parent, child in source:
        if parent is None:
            new_dict[child] = {}
        else:
            recursive_append(new_dict, parent, child)
    
    return new_dict
